#!/usr/bin/env python
# -*- coding: utf-8 -*-

from google.cloud.pubsub_v1.subscriber.message import Message
from google.cloud import pubsub

from connection import ServiceConnection

from datetime import datetime
from dateutil import parser as dtparse
from urllib import parse

import time
import json
import os

import sys
import traceback
import logging

logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))

# GLOBALS START

PROJECT = os.environ['PROJECT']
TOPIC = os.environ['TOPIC']
SUBSCRIPTION = os.environ['SUBSCRIPTION']

publisher = pubsub.PublisherClient()
publisher_path = 'projects/{}/topics/{}'.format(PROJECT, TOPIC)
subscriber = pubsub.SubscriberClient()
subscription_path = subscriber.subscription_path(PROJECT, SUBSCRIPTION)

# GLOBALS END

def _prevent_infinite_retry(timestamp:datetime) -> bool:
    """
    Method that ensures that a transfer job won't be retried indefinitely
    Timeout is fixed at 1 hour

    Args:
        timestamp (datetime): the timestamp to which the current time will be compared
    """
    event_time = dtparse.isoparse(timestamp)
    event_age = (datetime.now() - event_time).total_seconds()
    
    # Ignore events that are too old (1 hour)
    max_age = 3600
    if event_age > max_age:
        return True
    else:
        return False

def transfer_file(message:Message) -> str:
    """
    Main method that handles a Pub/Sub transfer job. This method, parses, connects and transfers
    the files from a source connection to a destination connection

    Args:
        message - the Pub/Sub message with the configurations for the transfer job.
    Refer to the README for details on the message format
    """
    try:
        # parsing the message
        pubsub_message = message.data.decode('utf-8')
        transfer_info = json.loads(pubsub_message)
        logging.info(transfer_info)
        # messages are ACK'd at the beggining. If the function errors during execution
        # the exception handling pushes a new message back onto the queue
        message.ack()
    except Exception as e:
        logging.error('Message could not be parsed: {}'.format(e.args))
        message.ack()
        return 'Error'

    # handling retries

    # if no event_date is given in the message, it will be marked as now
    # since retries re-send messages, this allows future messages to
    # actually have a timestamp
    if not transfer_info.get('event_date'):
        transfer_info['event_date'] = datetime.now().isoformat()

    if prevent_infinite_retry(transfer_info['event_date']):
        logging.error('{} aborted'.format(transfer_info))
        return 'Timeout'

    try:
        # creating the connection objects
        source = ServiceConnection(
            transfer_info['source_connection_string'],
            PROJECT,
            transfer_info.get('source_gcs_service_account'),
            transfer_info.get('source_s3_config_file'),
            transfer_info.get('compress_algorithm'),
            transfer_info.get('decompress_algorithm')
        )

        destination = source = ServiceConnection(
            transfer_info['destination_connection_string'],
            PROJECT,
            transfer_info.get('destination_gcs_service_account'),
            transfer_info.get('destination_s3_config_file')
        )

        # starting the transferrence, ensuring a cleanup if anything goes wrong
        try:
            source.transfer_files(destination, transfer_info.get('remove_file', False))
        except Exception as error:
            raise RuntimeError('Error during execution') from error
        finally:
            source.cleanup()
            destination.cleanup()

    except Exception as e:
        # if the transfer had problems, wait for 30s and resend the message
        # so that it can be retried
        logging.error('Error processing transfer - sending to retry')
        logging.error(e)
        logging.info(traceback.print_exception(*sys.exc_info()))
        time.sleep(30)
        publisher.publish(publisher_path, json.dumps(transfer_info).encode('utf-8'))
    
    return 'OK'

# handling the Pub/Sub subscription
streaming_pull_future = subscriber.subscribe(subscription_path, callback=transfer_file)
logging.info('Waiting for messages on {}'.format(subscription_path))

with subscriber:
    try:
        streaming_pull_future.result(timeout=7200)
    except Exception as e:
        streaming_pull_future.cancel()
        logging.error('Listening for messages on {} threw an exception: {}'.format(subscription_id, e))