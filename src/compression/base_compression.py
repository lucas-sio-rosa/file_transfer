# -*- coding: utf-8 -*-

import abc

class BaseCompression(abc.ABC):
    """
    Base abstract class that defines the compression methods to be used in the transfer jobs
    """
    def __init__(self):
        super().__init__()
    
    @abc.abstractclassmethod
    def compress_file(file_path:str) -> str:
        """
        Class method that takes a file path and compresses it
        Args
            file_path(str): the path to the file to be compressed
        
        Returns
            str: the file path of the compressed file
        """
        raise NotImplementedError('Abstract method')
    
    @abc.abstractclassmethod
    def decompress_file(file_path, encoding):
        """
        Class method that takes a file path and decompresses it
        Args
            file_path(str): the path to the file to be decompressed
        
        Returns
            str: the file path of the decompressed file
        
        Note
            The file to be decompressed must have only a single file inside
        """
        raise NotImplementedError('Abstract method')