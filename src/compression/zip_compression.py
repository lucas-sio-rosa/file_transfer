# -*- coding: utf-8 -*-

from compression.base_compression import BaseCompression
import zipfile as zp

import os
import logging

class ZipCompressClass(BaseCompression):
    """
    Concrete class for compression utilizing the Zip compression lib
    """
    def __init__(self):
        super().__init__()
    
    def compress_file(file_path):
        # preparing file paths and names
        dest_path = '{}.zip'.format(file_path.split('/')[-1])
        # compressing
        zip = zp.ZipFile(dest_path, 'w', compression=zp.ZIP_DEFLATED) 
        zip.write(file_path, arcname=file_path.split('/')[-1])
        zip.close()

        logging.info('Compressed file with zip: {}'.format(dest_path))

        return dest_path
    
    def decompress_file(file_path):
        # preparing file paths and names
        file_name = file_path.split('/')[-1]
        base_path = file_path[:file_path.rfind('/')]
        zip = zp.ZipFile(file_path, 'r', compression=zp.ZIP_DEFLATED)
        
        # ensuring that the zip contains only a single file
        if(len(zip.namelist())) > 1:
            raise Exception('Zip file must contain a single file')

        # decompressing
        for name in zip.namelist():
            dest_path = '{}/{}'.format(base_path, name)
            with open(dest_path, 'wb') as f:
                f.write(zip.read(name))

        logging.info('Decompressed file with zip: {}'.format(dest_path))

        return dest_path
