# -*- coding: utf-8 -*-

from compression.base_compression import BaseCompression
import gzip

import os
import logging

class GzipCompressClass(BaseCompression):
    """
    Concrete class for compression utilizing the GZip compression lib
    """
    def __init__(self):
        super().__init__()
    
    def compress_file(file_path):
        # preparing file paths and names
        dest_path = '{}.gz'.format(file_path)
        # compressing
        with open(file_path, 'rb') as r:
            with gzip.open(dest_path, 'wb') as f:
                f.write(r.read())
        
        logging.info('Compressed file with gzip: {}'.format(dest_path))

        return dest_path
    
    def decompress_file(file_path):
        # preparing file paths and names
        file_name = file_path.split('/')[-1]
        destination = file_name[:-3] if '.gz' in file_name else '{}01'.format(file_name)
        dest_path = '{}/{}'.format(file_path[:file_path.rfind('/')], destination)
        # decompressing
        with gzip.open(file_path, 'rb') as f:
            with open(dest_path, 'wb') as w:
                w.write(f.read())
        
        logging.info('Decompressed file with gzip: {}'.format(dest_path))
        return dest_path
