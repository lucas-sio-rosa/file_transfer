# -*- coding: utf-8 -*-

from transfer.gcs_transfer import GcsFileTransfer
from transfer.ftp_transfer import FtpFileTransfer
from transfer.sftp_transfer import SftpFileTransfer
from transfer.s3_transfer import S3FileTransfer

from compression.gzip_compression import GzipCompressClass
from compression.zip_compression import ZipCompressClass

from urllib import parse

import logging

class ServiceConnection():
    """
    Encapsulation of the connections used for the transfer jobs

    Parameters:
        connection_string (str): string with a URL-style connection string (e.g.: ftp://${IP}:${PORT}/${PATH}?username=${USER}&password=${PWD})
        project (str): the main project where the job is being executed on. Will also be used to download config files
        gcs_service_account (str): if given, points to a GCS file with a GCP service account JSON key for authentication
        s3_config_file (str): if given, points to a GCS file with AWS secrets to authenticate
        compression_algorithm (str): if given, will compress downloaded files with the chosen algorithm before sending them off
        decompression_algorithm (str): if given, will decompress downloaded files with the chosen algorithm before sending them off
    
    Notes:
        - If an S3 connection is used, the s3_config_file is mandatory
        - Both compression and decompression cannot be filled simultaneously
    """
    def __init__(
        self,
        connection_string:str,
        project:str,
        gcs_service_account:str = None,
        s3_config_file:str = None,
        compression_algorithm:str = None,
        decompression_algorithm:str = None
    ):
        # parsing and choosing connection
        parsed_connection = parse.urlparse(connection_string, allow_fragments=False)

        if parsed_connection.scheme == 'gs':
            self.conn = GcsFileTransfer(parsed_connection, project, gcs_service_account)
        elif parsed_connection.scheme == 'ftp':
            self.conn = FtpFileTransfer(parsed_connection)
        elif parsed_connection.scheme == 'sftp':
            self.conn = SftpFileTransfer(parsed_connection)
        elif parsed_connection.scheme == 's3':
            if project and s3_config_file:
                self.conn = S3FileTransfer(parsed_connection, project, s3_config_file)
            else:
                raise Exception('For S3 connections, project and config_file (AWS secrets) must be provided')
        else:
            raise LookupError('Type {} unsupported'.format(parsed_connection.scheme))

        # parsing and choosing compression
        if compression_algorithm and decompression_algorithm:
            raise Exception('Compression and decompression must not be set at the same time')
        else:
            algorithm = compression_algorithm if compression_algorithm else decompression_algorithm
            self.compression_operation = 'compress' if compression_algorithm else 'decompress'
            if algorithm == 'gzip':
                self.compression = GzipCompressClass
            elif algorithm == 'zip':
                self.compression = ZipCompressClass
            else:
                raise LookupError('Compression {} unsupported'.format(compression_algorithm))
        else:
            self.compression = None
            self.compression_operation = None
        
        self.conn.connect()
       
    def cleanup(self) -> None:
        """
        Calls the connection cleanup method to remove files from disk and,
        if needed, disconnect from the connection
        """
        self.conn.cleanup()
    
    def transfer_files(self, destination:ServiceConnection, remove_file:bool) -> None:
        """
        Method that transfer files, from the source, to the given destination
        Files are processed linearly, in a one-at-a-time fashion
        The system is not expected to deal with large files or a large number of them

        Args:
            destination (ServiceConnection): a ServiceConnection object that will be the destination of the files 
            remove_file (bool): indicates if the files are to be removed from the source after being transfered
        """
        # list files and transfer them one by one
        for file in self.conn.list_files():
            logging.info('Transferring file {} to destination {}'.format(file, self.conn.connection_string))

            try:
                file_name = self.conn.download_file(file)

                if self.compression_operation == 'decompress':
                    file_name = self.compression.decompress_file(file_name)

                if self.compression_operation == 'compress':
                    file_name = self.compression.compress_file(file_name)

                destination.upload_file(file_name)

                if remove_file:
                    self.conn.remove_file(file)
            except Exception as e:
                logging.error(e)