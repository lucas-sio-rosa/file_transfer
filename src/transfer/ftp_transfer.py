# -*- coding: utf-8 -*-

from transfer.base_transfer import FileTransfer
from urllib import parse
import ftplib
import fnmatch

import os
import logging

class FtpFileTransfer(FileTransfer):
    """
    Concrete FileTransfer for FTP connections
    """
    def __init__(self, connection_string):
        FileTransfer.__init__(self, connection_string)
        self.ftp = None
    
    def connect(self):
        self.ftp = ftplib.FTP()
        # checking if a port was specified to connect
        if self.connection_string.netloc.find(':') > -1:
            self.ftp.connect(self.connection_string.netloc.split(':')[0], int(self.connection_string.netloc.split(':')[1]))
        else:
            self.ftp.connect(self.connection_string.netloc)
        
        logging.info('Connected to FTP: {}'.format(self.connection_string.netloc))
        
        # parsing the query parameters to a dictionary for the login components
        auth_info = dict(parse.parse_qs(self.connection_string.query))
        self.ftp.login(auth_info['username'][0], auth_info['password'][0])
        
        logging.info('Login successfull')
    
    def disconnect(self):
        self.ftp.quit()
        
        logging.info('Disconnected from FTP: {}'.format(self.connection_string.netloc))
    
    def download_file(self, file_path: str):
        # creating the final file path
        file_name = file_path.split('/')[-1]
        path = '/tmp/{}/{}'.format(self.job_id, file_name)
        self.ftp.retrbinary('RETR {}'.format(file_path), open(path, 'wb').write)
        logging.info('Downloaded file {} from FTP {} successfully'.format(file_name, self.connection_string.netloc))

        return path
    
    def upload_file(self, file_path):
        file_name = file_path.split('/')[-1]
        
        # moving to the desired path
        for part in self.connection_string.path.split('/')[1:]:
            try:
                self.ftp.mkd(part)
            except ftplib.error_perm:
                pass

            self.ftp.cwd(part)

        # uploading file
        file = open(file_path, 'rb')
        self.ftp.storbinary('STOR {}'.format(file_name), file)
        file.close()

        logging.info('Uploaded file {} to FTP {} successfully'.format(file_path, self.connection_string.netloc))
    
    def remove_file(self, file_path):
        # creating the final file path
        self.ftp.delete(file_path)

        logging.info('Removed file {} from FTP successfully'.format(file_path, self.connection_string.netloc))

    def list_files(self):
        res = self.ftp.nlst(self.connection_string.path[:self.connection_string.path.rfind('/')])
        match = fnmatch.filter(res, '*{}'.format(self.connection_string.path[self.connection_string.path.rfind('/') + 1:]))
        logging.info('Matched files: {}'.format(', '.join(match)))

        return match
