# -*- coding: utf-8 -*-

from transfer.base_transfer import FileTransfer
from urllib import parse
import pysftp
import fnmatch

import os
import logging

class SftpFileTransfer(FileTransfer):
    """
    Concrete FileTransfer for SFTP connections
    """
    def __init__(self, connection_string):
        FileTransfer.__init__(self, connection_string)
        self.sftp = None
    
    def connect(self):
        # parsing the query parameters to a dictionary for the login components
        auth_info = dict(parse.parse_qs(self.connection_string.query))
        # ignoring known_hosts
        cnopts = pysftp.CnOpts()
        cnopts.hostkeys = None
        # checking if a port was specified to connect
        if self.connection_string.netloc.find(':') > -1:
            self.sftp = pysftp.Connection(
                self.connection_string.netloc.split(':')[0], port=int(self.connection_string.netloc.split(':')[1]),
                username=auth_info['username'][0], password=auth_info['password'][0], cnopts=cnopts
            )
        else:
            self.sftp = pysftp.Connection(
                self.connection_string.netloc, username=auth_info['username'][0], password=auth_info['password'][0], cnopts=cnopts
            )

        logging.info('Connected to SFTP: {}'.format(self.connection_string.netloc))
    
    def disconnect(self):
        self.sftp.close()

        logging.info('Disconnected from SFTP: {}'.format(self.connection_string.netloc))
    
    def download_file(self, file_path: str):
        # creating the final file path
        file_name = file_path.split('/')[-1]
        path = '/tmp/{}/{}'.format(self.job_id, file_name)
        self.sftp.get(file_path, localpath=path)
        logging.info('Downloaded file {} from FTP {} successfully'.format(file_name, self.connection_string.netloc))

        return path
    
    def upload_file(self, file_path):
        file_name = file_path.split('/')[-1]
        # moving to the desired path
        self.sftp.makedirs(self.connection_string.path)
        # uploading file
        remote_path = '{}{}{}'.format(self.connection_string.path, '' if self.connection_string.path.endswith('/') else '/', file_name)
        self.sftp.put(file_path, remote_path)

        logging.info('Uploaded file {} to SFTP {} successfully'.format(file_path, self.connection_string.netloc))
    
    def remove_file(self, file_path):
        # creating the final file path
        self.sftp.remove(file_path)
        
        logging.info('Removed file {} from SFTP successfully'.format(file_path, self.connection_string.netloc))

    def list_files(self):
        folder_path = self.connection_string.path[:self.connection_string.path.rfind('/')]
        # listdir does not include the folder path, so we manually add it
        res = ['{}/{}'.format(folder_path, f) for f in self.sftp.listdir(folder_path)]
        match = fnmatch.filter(res, '*' + self.connection_string.path[self.connection_string.path.rfind('/'):])
        logging.info('Matched files: {}'.format(', '.join(match)))

        return match
