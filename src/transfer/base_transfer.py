# -*- coding: utf-8 -*-

from urllib import parse
import uuid
import abc
import os
import shutil

from typing import NamedTuple, List
import logging

class FileTransfer(abc.ABC):
    """
    Base abstract class that defines the operations on files for transfer jobs

    Args
        connection_string (NamedTuple): the parsed connection string
    """
    def __init__(self, connection_string: NamedTuple):
        self.connection_string = connection_string
        self.job_id = uuid.uuid4()
        # creating the temporary workfolder for this job
        os.mkdir('/tmp/{}'.format(self.job_id))

        logging.info('Job ID: {}'.format(self.job_id))
        
    @abc.abstractmethod
    def connect(self):
        """
        Method that connects to the service
        """
        raise NotImplementedError('Abstract method')
    
    @abc.abstractmethod
    def download_file(self, file_path: str) -> str:
        """
        Method that downloads a file from the service to the local workspace folder

        Args
            file_path (str): the path to the file to be downloaded
        
        Returns
            The file path to the downloaded file in the workspace folder
        """
        raise NotImplementedError('Abstract method')
    
    @abc.abstractmethod
    def upload_file(self, file_path: str) -> None:
        """
        Method that uploads a file from the workspace folder to the remote service

        Args
            file_path (str): the path to which the file will be uploaded to
        
        Note
            The file name is derived from the file_path parameter, no control is given to rename it
        """
        raise NotImplementedError('Abstract method')
    
    @abc.abstractmethod
    def list_files(self, file_path: str) -> List[str]:
        """
        Method that lists the files from the remote service path, and filtering based on fnmatch

        Args
            file_path (str): the remote path to be listed and filtered
        
        Returns
            List[str]: list of matched file paths
        
        Note
            Filtering should be made using fnmatch rather than relying on the service filtering capabilities
            This gives the service a more consistent filtering capacity
        """
        raise NotImplementedError('Abstract method')
    
    @abc.abstractmethod
    def remove_file(self, file_path: str) -> None:
        """
        Method that removes a file from the remote service

        Args
            file_path (str): the path to the file to be removed from the remote service
        """
        raise NotImplementedError('Abstract method')

    @abc.abstractmethod
    def disconnect(self) -> None:
        """
        Method that disconnects from the remote service
        """
        raise NotImplementedError('Abstract method')

    def cleanup(self) -> None:
        """
        Method that closes the connection and cleans up the workspace folder
        """
        self.disconnect()
        shutil.rmtree('/tmp/{}'.format(self.job_id))