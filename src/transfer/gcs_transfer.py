# -*- coding: utf-8 -*-

from transfer.base_transfer import FileTransfer
from google.cloud import storage
from urllib import parse
import fnmatch

import os
import logging

class GcsFileTransfer(FileTransfer):
    """
    Concrete FileTransfer for GCS connections
    """
    def __init__(self, connection_string:str, project:str, service_account:str = None):
        FileTransfer.__init__(self, connection_string)
        self.gcs = None
        self.bucket = None
        self.project = project
        self.service_account = service_account
    
    def connect(self):
        if self.service_account:
            self.auth_file = '/tmp/{0}/{0}.json'.format(self.job_id)
            client = storage.Client(self.project)

            with open(self.auth_file, 'wb') as f:
                client.download_blob_to_file(self.service_account, f)
            
            self.gcs = storage.Client.from_service_account_json(self.auth_file)
        else:
            self.gcs = storage.Client(self.project)

        self.bucket = self.gcs.get_bucket(self.connection_string.netloc)

        logging.info('Connected to GCS on project: {}'.format(self.project))
    
    def download_file(self, file_path):
        # removing the leading / so as to not create a folder with it
        base_path = '{}/'.format(self.connection_string.path[1:self.connection_string.path.rfind('/')])
        base_path = base_path if base_path != '/' else ''

        file_name = file_path.split('/')[-1]
        dest_path = '/tmp/{}/{}'.format(self.job_id, file_name)

        blob = self.bucket.blob('{}{}'.format(base_path, file_name))
        blob.download_to_filename(dest_path)
        logging.info('Downloaded file {} from bucket {} successfully'.format(file_path, self.bucket.name))

        return dest_path
    
    def upload_file(self, file_path):
        # creating the final file path
        base_path = self.connection_string.path[1:]
        base_path = base_path if base_path.endswith('/') or base_path == '' else '{}/'.format(base_path)
        path = '{}{}'.format(base_path, file_path[file_path.rfind('/') + 1:])
        
        blob = self.bucket.blob(path)
        blob.upload_from_filename(file_path)
        logging.info('Uploaded file {} to bucket {} successfully'.format(file_path, self.bucket.name))
    
    def remove_file(self, file_path):
        self.bucket.delete_blob(file_path)
        logging.info('Removed file {} from bucket {} successfully'.format(file_path, self.bucket.name))

    def list_files(self):
        # from the blob file list return only the file names
        files = [f.name for f in self.bucket.list_blobs(prefix=self.connection_string.path[1:self.connection_string.path.rfind('/')])]
        matches = fnmatch.filter(files, '*{}'.format(self.connection_string.path[self.connection_string.path.rfind('/') + 1:]))
        logging.info('Matched files: {}'.format(', '.join(matches)))
        
        return matches
    
    def disconnect(self):
        # gcs client does not require an explicit disconnect
        if self.service_account:
            os.remove(self.auth_file)
        logging.info('Disconnected from GCS project {}'.format(self.project))