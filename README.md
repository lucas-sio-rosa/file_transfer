# Simple File Transfer

This project aims to create a simple service for transfering files between services, while being hosted in a GCP environment. 
Based on a JSON message, posted in a Pub/Sub topic, the service will list files in the source connection and transfer them
to the destination one by one. Optionally, files can be compressed or decompressed before being sent.
Naturally, this service is not meant to transfer high volumes of data, be it as hundreds of files or several gigabytes in single files.

The file listing is not made on the remote service side, because there's no consistency on the filtering rules and capabilities
 (i.e. FTP servers are much more limited than a GCS bucket, for example). Because of that, the entire folder is listed
 and then python's fnmatch is leveraged to filter according to the given path. While this ensures consistency, folders
 with a large number of files might lead to slowdowns in the execution.

## Process

In order for the execution to run, every connection gets a GUID when it is created. This GUID will be used to create
a temporary working folder in `/tmp`. This folder stages downloaded files, (de)compressed files, etc, so that they can
be uploaded later. This allows for larger files to be processed without the need to be kept in memory through the entire process.
Also, this makes creating new services easier, since sources and destinations interact through the filesystem, so there's no risk
of compatibility issues between them.

## Configuration

In order to execute, the following environment variables are needed:

* **PROJECT** - the GCP project where the code is being executed
* **TOPIC** - the topic that the subscription and the retry messages are located
* **SUBSCRIPTION** - the subscription to be used by the code

## Execution

The code expects a JSON message with the following pattern:

```json
{
    "source_connection_string": "URL-style connection string (e.g.: ftp://${IP}:${PORT}/${PATH}?username=${USER}&password=${PWD})",
    "source_gcs_service_account": "(optional) GCS URI with a JSON service-account key to be used to interact with the source bucket",
    "source_s3_config_file": "(required for S3) a JSON file with the authentication info to connect to the source S3 bucket",
    "compress_algorithm": "(optional) the algorithm to be used for compressing the files before transfer",
    "decompress_algorithm": "(optional) the algorithm to be used for decompressing the files before transfer",
    "destination_connection_string": "URL-style connection string (e.g.: ftp://${IP}:${PORT}/${PATH}?username=${USER}&password=${PWD})",
    "destination_gcs_service_account": "(optional) GCS URI with a JSON service-account key to be used to interact with the destination bucket",
    "destination_s3_config_file": "(required for S3) a JSON file with the authentication info to connect to the destination S3 bucket",
    "event_date": "(optional) a date in ISO format stating when the request was first made - used to prevent infnite retries",
    "remove_file": "(optional) flag indicating if the files are to be removed from the source after being transfered"
}
```

## Examples

Some examples to help:

Move from FTP to GCS, using a service account, compressing files and removing files from source

```json
{
    "source_connection_string": "ftp://172.17.0.2/a*?username=admin&password=admin",
    "source_gcs_service_account": "gs://teste-dataflow/service.json",
    "compress_algorithm": "zip",
    "destination_connection_string": "gs://teste-dataflow-2/temp",
    "remove_file": true
}
```

Move from S3 to SFTP, and decompressing:
```json
{
    "source_connection_string": "s3://testes-lucas/*",
    "source_config_file": "gs://teste-dataflow/s3.json",
    "decompress_algorithm": "zip",
    "destination_connection_string": "sftp://172.17.0.3/admin/temp/teste/path?username=admin&password=admin"
}
```

## Supported Connections

Currently supported are:

* **GCS** - `gs://${BUCKET}/${PATH}`
* **S3** - `s3://${BUCKET}/${PATH}`
* **FTP** - `ftp://${IP}:${PORT}/${PATH}?username=${user}&password=${pwd}`
* **SFTP** - `sftp://${IP}:${PORT}/${PATH}?username=${user}&password=${pwd}`

## Deployment

The code is built with Docker, and ideally should be deployed to a VM:
```sh
docker run --name file_transfer -d -e PROJECT=${PROJECT} -e TOPIC=${TOPIC_NAME} -e SUBSCRIPTION=${SUBSCRIPTION_NAME} file_transfer
```

GCP allows deploying a container inside a VM, which helps in getting it set up easily.
Make sure the disk is a reasonable size, as it will be used by the container for its work folders.

## Author

* [**Lucas Rosa**](https://bitbucket.org/dotz-lucas-rosa/)
