FROM python:3.8-alpine

# required libs (mainly for gcrpcio and boto)
RUN apk update && apk add --no-cache linux-headers gcc g++ make libressl-dev musl-dev libffi-dev

# installing the python requirements
COPY requirements.txt /

RUN set -ex && \
    pip install -r /requirements.txt

# adding the GCP service account credentials
ADD credentials /opt/file_transfer

ENV GOOGLE_APPLICATION_CREDENTIALS /opt/file_transfer/cred.json

# adding source code
ADD src /opt/file_transfer/bin

# executing the code
RUN chmod u+x /opt/file_transfer/bin/worker.py

CMD ["/opt/file_transfer/bin/worker.py"]